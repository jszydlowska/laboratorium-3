package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab3PbaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab3PbaApplication.class, args);
	}

}
